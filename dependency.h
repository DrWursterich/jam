
#ifndef DEPENDENCY_H
#define DEPENDENCY_H

#include <stdbool.h>

#define CONFIG_FILE "jam.ini"
#define PATH_SEPARATOR "/"

#define DOWNLOAD_OUTPUT_DIR ".jamdependencies"
#define COMMAND_DOWNLOAD_OPEN "git clone -q -c advice.detachedHead=false --depth 1 \""
#define COMMAND_DOWNLOAD_CLOSE ".git\""
#define COMMAND_DOWNLOAD_OUTPUT_DIR_OPEN (" \"" DOWNLOAD_OUTPUT_DIR PATH_SEPARATOR)
#define COMMAND_DOWNLOAD_OUTPUT_DIR_CLOSE "\""
#define COMMAND_DOWNLOAD_VERSION_OPEN " -b \""
#define COMMAND_DOWNLOAD_VERSION_CLOSE "\""

/**
 * Existance and permissions of a file on the file system.
 */
typedef struct {
	bool exists;
	bool canRead;
	bool canWrite;
	bool canExecute;
} FileProperties;

/**
 * A Dependency of an artifact.
 * An artifact is defined as either the project or another Dependency.
 * All direct dependencies form a Linked-List.
 * The Dependencies of a Dependency have to be accessed via their Config,
 * which holds a reference to the first element of their Dependencies-List.
 */
struct Dependency {
	char *name;
	char *url;
	char *version;
	struct Dependency *nextDependency;
};

FileProperties *getFilePropertiesOf(const char *file);

struct Dependency *createDependency();
void destroyDependency(struct Dependency *dependency);

char *getPathToDependency(struct Dependency *dependency);
char *getConfigPathForDependency(struct Dependency *dependency);

char *buildDownloadCommandFromDependency(struct Dependency *dependency);

void downloadDependency(struct Dependency *dependency);
void buildDependency(struct Dependency *dependency);

#endif

