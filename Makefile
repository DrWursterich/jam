
CC?=gcc
CFLAGS+=-pedantic -Wall -Werror

ifeq ($(PREFIX),)
    PREFIX := /usr/local
endif

jam: jam.c jam.h config.o dependency.o javaproperties.o stringutils.o
	$(CC) $(CFLAGS) -o $@ -g $^
	$(RM) *.o

stringutils.o: stringutils.c stringutils.h
	$(CC) $(CFLAGS) -o $@ -g -c $<

config.o: config.c config.h dependency.o javaproperties.o
	$(CC) $(CFLAGS) -o $@ -g -c $<

dependency.o: dependency.c dependency.h
	$(CC) $(CFLAGS) -o $@ -g -c $<

javaproperties.o: javaproperties.c javaproperties.h
	$(CC) $(CFLAGS) -o $@ -g -c $<

.PHONY: install
install: jam
	install -d $(DESTDIR)$(PREFIX)/bin/
	install -m 751 jam $(DESTDIR)$(PREFIX)/bin/
	$(RM) jam

.PHONY: uninstall
uninstall:
	$(RM) $(DESTDIR)$(PREFIX)/bin/jam

.PHONY: clean
clean:
	$(RM) $(TARGET)

