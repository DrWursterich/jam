
#ifndef JAVAPROPERTIES_H
#define JAVAPROPERTIES_H

#define CLASSPATH_SEPARATOR ":"
#define MODULE_PATH_SEPARATOR ";"
#define ADD_MODULES_SEPARATOR ","

/**
 * The Context of a (part of a) Java program
 */
typedef struct {
	char *classpath;
	char *modulePath;
	char *addModules;
	char *srcFolder;
	char *mainClass;
} JavaContext;

/**
 * All properties required to compile/run/test the project
 */
typedef struct {
	char *srcFolders;
	char *binFolder;
	char *arguments;
	JavaContext *runContext;
	JavaContext *testContext;
} JavaProperties;

JavaContext *createJavaContext();
void destroyJavaContext();

JavaProperties *createJavaProperties();
void destroyJavaProperties(JavaProperties *properties);

void mergeJavaContextInto(const JavaContext *source, JavaContext *target);
void mergeJavaPropertiesInto(const JavaProperties *source, JavaProperties *target);

#endif

