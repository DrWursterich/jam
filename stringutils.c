
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>
#include <string.h>

#include "stringutils.h"

/**
 * Concatinates all given strings into a new one,
 * separated by the given separator char*.
 * The amount of strings to concatinate is also required.
 * This method is NULL-save. It may return NULL,
 * if the resulting string would be empty.
 * Otherwise memory will be allocated for the result,
 * so it has to be freed manually.
 */
char *joinStrings(const char *separator, const int argc, ...) {
	va_list args;
	char *arg;
	int argSize;
	int i;
	char *result;
	int separatorSize = separator != NULL ? strlen(separator) : 0;
	bool hasSeparator = separatorSize > 0;
	bool isFirst = true;
	int size = 1;
	va_start(args, argc);
	for (i = 0; i < argc; i++) {
		arg = va_arg(args, char*);
		if (arg == NULL) {
			continue;
		}
		argSize = strlen(arg);
		if (argSize > 0) {
			if (hasSeparator && !isFirst) {
				size += separatorSize;
			} else {
				isFirst = false;
			}
			size += separatorSize + argSize;
		}
	}
	va_end(args);
	if (size == 1) {
		return NULL;
	}
	result = malloc(sizeof(char) * size);
	isFirst = true;
	va_start(args, argc);
	for (i = 0; i < argc; i++) {
		arg = va_arg(args, char*);
		if (arg == NULL
				|| strlen(arg) == 0) {
			continue;
		}
		if (isFirst) {
			strcpy(result, arg);
			isFirst = false;
		} else {
			if (hasSeparator) {
				strcat(result, separator);
			}
			strcat(result, arg);
		}
	}
	va_end(args);
	return result;
}

/**
 * Creates new a string from the input,
 * that is suitable for system calls to java.
 * The specification of the input is as follows:
 * - arguments are separated by colons
 * - colons can be escaped by another colon
 * So given the input 'groupid::artifactid:groupid2::artifactid2'
 * the result would be ' "groupid:artifactid" "groupid2:artifactid2"'
 * Memory will be allocated for the result,
 * so it has to be freed manually.
 */
char *transformArgumentsString(const char *arguments) {
	int size = 1
			+ strlen(COMMAND_ARGUMENT_OPEN)
			+ strlen(arguments)
			+ strlen(COMMAND_ARGUMENT_CLOSE);
	char *result = malloc(sizeof(char) * size);
	bool lastCharWasColon = false;
	int resultLength;
	int additionalChars;
	strcpy(result, COMMAND_ARGUMENT_OPEN);
	resultLength = strlen(result);
	while (*arguments != '\0') {
		if (lastCharWasColon) {
			if (*arguments == ':') {
				result[resultLength] = ':';
				result[++resultLength] = 0;
			} else {
				additionalChars = strlen(COMMAND_ARGUMENT_CLOSE)
						+ strlen(COMMAND_ARGUMENT_OPEN);
				size += additionalChars;
				resultLength += additionalChars;
				result = realloc(result, sizeof(char) * size);
				strcat(result, COMMAND_ARGUMENT_CLOSE);
				strcat(result, COMMAND_ARGUMENT_OPEN);
				arguments--;
			}
			lastCharWasColon = false;
		} else if (*arguments == ':') {
			lastCharWasColon = true;
		} else {
			result[resultLength] = *arguments;
			result[++resultLength] = 0;
			lastCharWasColon = false;
		}
		arguments++;
	}
	strcat(result, COMMAND_ARGUMENT_CLOSE);
	return result;
}

/**
 * Allocates memory and fills it with a copy of the source.
 * If the target is not a NULL pointer, it is freed beforehand.
 * Has to be freed manually.
 */
void copyStringInto(const char *source, char **target) {
	if (*target != NULL) {
		free(*target);
	}
	*target = malloc(sizeof(char) * (strlen(source) + 1));
	strcpy(*target, source);
}

/**
 * Merges the given source into the given target by appending it.
 * If both are present, they will be separated by the given separator
 * (if the separator itself is not NULL).
 * If only one is present, the target will be only the present value.
 * Does nothing, if the source is NULL.
 */
void mergeStringInto(const char *source, const char *separator, char **target) {
	const int size = 1
			+ (source != NULL ? strlen(source) : 0)
			+ (separator != NULL ? strlen(separator) : 0)
			+ (*target != NULL ? strlen(*target) : 0);
	if (*target != NULL) {
		if (source != NULL) {
			*target = realloc(*target, sizeof(char) * size);
			if (separator != NULL) {
				strcat(*target, separator);
			}
			strcat(*target, source);
		}
	} else if (source != NULL) {
		*target = malloc(sizeof(char) * size);
		strcpy(*target, source);
	}
}

/**
 * Prepends the given string with the given prefix.
 * The memory of the string will be reallocated,
 * so literals are not allowed.
 */
void prependString(char *string, const char *prefix) {
	int stringSize = strlen(string);
	int prefixSize = strlen(prefix);
	string = realloc(string, sizeof(char) * (stringSize + prefixSize) + 1);
	memmove(string + prefixSize, string, stringSize + 1);
	memcpy(string, prefix, prefixSize);
}

