
#ifndef CONFIG_H
#define CONFIG_H

#include "javaproperties.h"
#include "dependency.h"

#define MAX_CONFIG_PROPERTY_CHARS 128
#define DEFAULT_SRC_FOLDER "."

/**
 * An indicator of which part of a jam.ini config
 * file is currently beeing parsed.
 */
enum ReadingMode {
	NONE,
	COMMENT,
	SECTION,
	AFTER_SECTION,
	PROPERTY_NAME,
	PROPERTY_VALUE
};

/**
 * The accumulated data of a jam.ini config file.
 * The direct Dependencies are stored in a Linked-List.
 * Dependencies of Dependencies have to be accessed via
 * their own Configs.
 */
typedef struct {
	char *package;
	char *project;
	char *version;
	char *author;
	JavaProperties *javaProperties;
	struct Dependency *dependency;
} Config;

Config *createConfig();
void destroyConfig(Config *config);
bool setConfigValueByName(
		Config *config,
		const char *fieldName,
		const char *value);

Config *getConfigOfFile(char *fileName);
Config *getConfigOfProject();
Config *getConfigOfDependency(struct Dependency *dependency);

void prependRelativeClasspathEntries(char *classpath, const char *prefix);

JavaProperties *buildMergedJavaProperties(Config *config);

#endif

