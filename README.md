# JAM - The Java Artifact Manager

![JAM logo and text](/images/logo-and-text.png)

JAM is a minmalistc, git-based Artifact Manager for Java-Projects.
Written in strict ISO C with only POSIX dependencies, JAM is lightweight and easy to build and install.

## Getting started

First of all, make sure you have the following programs installed and in your `PATH` environment variable:
* git
* javac
* java

Go into your project and run `jam init`.
This will create a `jam.ini` file, in which we can configure everything necessary.
Look into [configuration](#configuration) and define the important properties.
Then you can run `jam build` to compile and `jam run` to execute it.

If your project depends on other projects, these will have to be on git too.
They will also need atleast one tag. Theoretically everything works, but it is strongly recommended to be a version following the [SemVer specification](https://semver.org/).
In this tag has to be another `jam.ini` file in your repository, describing the properties of that project.

Then you can enter the dependency inside of your projects `jam.ini` and continue using `jam build run`.

## Configuration

All configuration is done inside a projects `jam.ini`. There is no global configuration or homedirectory folder.

The file follows these specifications:
```ini
; Everything after a semicolon is considered a comment
		; Every line can have preceeding whitespace
; Empty lines are ignored

; Key-Value pairs look like this:
key=value

; Whitespaces after the first non-whitespace character are NOT ignored
key = value ; results in "key "=" value "

; Values can currently not hold semicolns

; Sections look like this:
[section]

; Keys before the first section are considered global
; Keys after a section belong to the previous

; Each section defines a dependency
; The name in between the brackets is not important,
; but should be, what best describes the dependency

; Bash variables, parameter expansion, and variable- and command-substitution
; will be evaluated in most values. This is a known security issue and will
; be changed in the near future.
; Until than be warry of what dependecies you include and only run jam with
; the bare minimum of permissions.
```

Here are all currently possible keys:

### Global Keys

| Key				| Example Value					| Description																																						|
| :---------------- | :---------------------------- | :---------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| package			| de.schaeper					| The highest common package of your project. This properties is not in use yet and might change!																	|
| project			| conditionals					| The name of your project. Not used yet.																															|
| version			| 1.0.1							| The current version. This is also not used yet, but it will be in the future, so keep in in sync with your git tag!												|
| author			| schaeper						| The author(s) of the project																																		|
| classpath			| lib/main/jdbc.jar				| Additional classpath entries seperated by colons. Source-Folders do not have to be included here.																	|
| binFolder			| bin							| the folder, in with the `.class` files should be compiled to																										|
| modulePath		| $PATH_TO_FX					| The value, that should be passed via the `--module-path` flag to the java or javac command.																		|
| addModules		| javafx.base					| The value, that should be passed via the `--add-modules` flag to the java or javac command.																		|
| srcFolder			| src/main/java					| The Source-Folder																																					|
| mainClass			| de.schaeper.Application		| The Main-Class																																					|
| testClasspath		| lib/test/junit-4.jar			| Additional classpath entries for tests. Source-Folders and values from the `classpath` do not have to be included here.											|
| testModulePath	| $PATH_TO_TEST_FRAMEWORK		| Additional values, that should be passed via the `--module-path` flag to the java or javac command. Values from the `modulePath` key will be merged with these.	|
| testAddModules	| testframework.assertions		| Additional values, that should be passed via the `--add-modules` flag to the java or javac command. Values from the `addModules` key will be merged with these.	|
| testFolder		| src/test/java					| The Source-Folder of the projects test classes																													|
| testClass			| org.junit.runner.JUnitCore	| The main class for the `test` command																																|

### Dependency Keys
| Key		| Example Value																				| Description													|
| :-------- | :---------------------------------------------------------------------------------------- | :------------------------------------------------------------ |
| version	| 1.0.1																						| The version, in which the dependen project is required		|
| url		| [git@gitlab.com:DrWursterich/conditionals](https://gitlab.com/DrWursterich/conditionals)	| The HTTPS- or SSH-URL of the git repository (without `.git`)	|

## The Command-Line Interface

As described by `jam --help`:
```bash
Usage: jam [OPTION...]
            [<init [INIT-OPTION...] | build [BUILD-OPTION...] | test
            [TEST-OPTION...] | run [RUN-OPTION...]>...]
jam -- The Java Artifact Manager

 test
  -a, --args=ARGS            use the given, colon-separated arguments for the
                             java invocation. Colons are escaped by an
                             additional colon

 run
      --args=ARGS            use the given, colon-separated arguments for the
                             java invocation. Colons are escaped by an
                             additional colon

 global

  -?, --help                 Give this help list
      --usage                Give a short usage message
  -V, --version              Print program version

Mandatory or optional arguments to long options are also mandatory or optional
for any corresponding short options.
```

**Syntax wise the order of flags and commands does matters.**
Flags before the first command (if present) are considered global, others belong to the previous command.
Commands are executed in the order given from left to right.

## Installation

Simply do:

```bash
git clone https://gitlab.com/DrWursterich/jam.git
cd jam
sudo make install
```

To uninstall it, run

```bash
sudo make uninstall
```

## Try It Out!

Pull from [jam-test-b](https://gitlab.com/DrWursterich/jam-test-b) and run `jam build`.
It will download [jam-test-a](https://gitlab.com/DrWursterich/jam-test-a) and compile both.
Then try `jam run` and watch it print out __Hello, World__!

## Contributing

Development is as straight forward as it gets: Fork this Repository, make your changes, build it with `make` (see [Installing](#installing)) and test it.
If you're happy with it, feel free to open a PR - All help is welcome!

