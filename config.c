
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>

#include "config.h"
#include "stringutils.h"

/**
 * Allocates memory for a Config and creates a empty one
 * together with empty JavaProperties.
 * Has to be freed manually.
 */
Config *createConfig() {
	Config *config = malloc(sizeof(Config));
	config->package = NULL;
	config->project = NULL;
	config->version = NULL;
	config->author = NULL;
	config->javaProperties = createJavaProperties();
	config->dependency = NULL;
	return config;
}

/**
 * Frees the allocated memory of a Config and all of its values,
 * including the Linked-List of Dependencies and all of their values.
 */
void destroyConfig(Config *config) {
	if (config->package != NULL) {
		free(config->package);
	}
	if (config->project != NULL) {
		free(config->project);
	}
	if (config->version != NULL) {
		free(config->version);
	}
	if (config->author != NULL) {
		free(config->author);
	}
	if (config->javaProperties != NULL) {
		destroyJavaProperties(config->javaProperties);
	}
	if (config->dependency != NULL) {
		destroyDependency(config->dependency);
	}
	free(config);
}

/**
 * Copies the given value into a field in the given config,
 * identified by the given field-name.
 * The field-names are the global properties of the jam.ini config file.
 * Present values will be overwritten by this.
 * Returns true, if a field was found and set, false otherwise.
 */
bool setConfigValueByName(
		Config *config,
		const char *fieldName,
		const char *value) {
	if (strcmp(fieldName, "package") == 0) {
		copyStringInto(value, &config->package);
	} else if (strcmp(fieldName, "project") == 0) {
		copyStringInto(value, &config->project);
	} else if (strcmp(fieldName, "version") == 0) {
		copyStringInto(value, &config->version);
	} else if (strcmp(fieldName, "author") == 0) {
		copyStringInto(value, &config->author);
	} else if (strcmp(fieldName, "binFolder") == 0) {
		copyStringInto(value, &config->javaProperties->binFolder);
	} else if (strcmp(fieldName, "classpath") == 0) {
		copyStringInto(value, &config->javaProperties->runContext->classpath);
	} else if (strcmp(fieldName, "modulePath") == 0) {
		copyStringInto(value, &config->javaProperties->runContext->modulePath);
	} else if (strcmp(fieldName, "addModules") == 0) {
		copyStringInto(value, &config->javaProperties->runContext->addModules);
	} else if (strcmp(fieldName, "srcFolder") == 0) {
		copyStringInto(value, &config->javaProperties->runContext->srcFolder);
		copyStringInto(value, &config->javaProperties->srcFolders);
	} else if (strcmp(fieldName, "mainClass") == 0) {
		copyStringInto(value, &config->javaProperties->runContext->mainClass);
	} else if (strcmp(fieldName, "testClasspath") == 0) {
		copyStringInto(value, &config->javaProperties->testContext->classpath);
	} else if (strcmp(fieldName, "testModulePath") == 0) {
		copyStringInto(value, &config->javaProperties->testContext->modulePath);
	} else if (strcmp(fieldName, "testAddModules") == 0) {
		copyStringInto(value, &config->javaProperties->testContext->addModules);
	} else if (strcmp(fieldName, "testFolder") == 0) {
		copyStringInto(value, &config->javaProperties->testContext->srcFolder);
	} else if (strcmp(fieldName, "testClass") == 0) {
		copyStringInto(value, &config->javaProperties->testContext->mainClass);
	} else {
		return false;
	}
	return true;
}

/**
 * Parses a config file and populates a new Config with the resulting data.
 * Aborts, if the file is inaccessable or does not follow the required syntax.
 * Since memory will be allocated for the result, it has to be freed manually.
 */
Config *getConfigOfFile(char *fileName) {
	FileProperties *fileProperties = getFilePropertiesOf(fileName);
	FILE *file = NULL;
	Config *config;
	char readCharacter;
	char propertyName[MAX_CONFIG_PROPERTY_CHARS] = "";
	char propertyValue[MAX_CONFIG_PROPERTY_CHARS] = "";
	char sectionName[MAX_CONFIG_PROPERTY_CHARS] = "";
	int propertyNameChars = 0;
	int propertyValueChars = 0;
	int sectionNameChars = 0;
	enum ReadingMode readingMode = PROPERTY_NAME;
	bool hasReadSection = false;
	bool errorOccured = false;
	bool hasOnlyReadWhitespaceOnCurrentLine = true;
	struct Dependency *dependency = NULL;
	if (!fileProperties->exists) {
		fprintf(stderr, "no %s found\n", fileName);
		free(fileProperties);
		exit(EXIT_FAILURE);
	}
	if (!fileProperties->canRead) {
		fprintf(stderr, "cannot read %s\n", fileName);
		free(fileProperties);
		exit(EXIT_FAILURE);
	}
	free(fileProperties);
	file = fopen(fileName, "r");
	if (file == NULL) {
		fprintf(stderr, "cannot open %s\n", fileName);
		exit(EXIT_FAILURE);
	}
	config = createConfig();
	do {
		readCharacter = (char)fgetc(file);
		if (readCharacter == ';') {
			readingMode = COMMENT;
		} else if (readCharacter == '\n') {
			if (!hasOnlyReadWhitespaceOnCurrentLine) {
				if (readingMode == PROPERTY_VALUE) {
					propertyName[propertyNameChars] = '\0';
					propertyValue[propertyValueChars] = '\0';
					if (!hasReadSection) {
						if (!setConfigValueByName(
								config,
								propertyName,
								propertyValue)) {
							fprintf(
									stderr,
									"unknown global property \"%s\"\n",
									propertyName);
							errorOccured = true;
							break;
						}
					} else {
						if (strcmp(propertyName, "url") == 0) {
							copyStringInto(propertyValue, &dependency->url);
						} else if (strcmp(propertyName, "version") == 0) {
							copyStringInto(propertyValue, &dependency->version);
						} else {
							fprintf(
									stderr,
									"unknown property \"%s\"\n",
									propertyName);
							errorOccured = true;
							break;
						}
					}
					propertyValue[0] = '\0';
					propertyValueChars = 0;
					propertyName[0] = '\0';
					propertyNameChars = 0;
				}
				readingMode = PROPERTY_NAME;
				hasOnlyReadWhitespaceOnCurrentLine = true;
			}
		} else if (readingMode == COMMENT) {
			/* ignore comments */
		} else if (readingMode == AFTER_SECTION) {
			if (readCharacter !=
					' ' && readCharacter != '\t') {
				fprintf(
						stderr,
						"trailing characters after section definition\n");
				errorOccured = true;
			}
		} else if (readCharacter == '=') {
			if (hasOnlyReadWhitespaceOnCurrentLine) {
				fprintf(stderr, "propertyName cannot be empty\n");
				errorOccured = true;
				break;
			}
			readingMode = PROPERTY_VALUE;
		} else if (readCharacter == '[') {
			if (!hasOnlyReadWhitespaceOnCurrentLine) {
				fprintf(
						stderr,
						"leading characters before section definition\n");
				errorOccured = true;
				break;
			}
			sectionName[0] = '\0';
			sectionNameChars = 0;
			readingMode = SECTION;
		} else if (readCharacter == ']') {
			if (readingMode != SECTION) {
				fprintf(stderr, "no section definition is closable\n");
				errorOccured = true;
				break;
			}
			if (sectionNameChars == 0) {
				fprintf(stderr, "section names cannot be empty\n");
				errorOccured = true;
				break;
			}
			if (dependency == NULL) {
				config->dependency = createDependency();
				dependency = config->dependency;
			} else {
				dependency->nextDependency = createDependency();
				dependency = dependency->nextDependency;
			}
			sectionName[sectionNameChars] = '\0';
			copyStringInto(sectionName, &dependency->name);
			readingMode = AFTER_SECTION;
			hasReadSection = true;
		} else if (readingMode == SECTION) {
			if (sectionNameChars < MAX_CONFIG_PROPERTY_CHARS + 1) {
				sectionName[sectionNameChars++] = readCharacter;
			}
		} else if (readingMode == PROPERTY_NAME) {
			if (propertyNameChars < MAX_CONFIG_PROPERTY_CHARS + 1) {
				propertyName[propertyNameChars++] = readCharacter;
			}
		} else {
			if (propertyValueChars < MAX_CONFIG_PROPERTY_CHARS + 1) {
				propertyValue[propertyValueChars++] = readCharacter;
			}
		}
		if (hasOnlyReadWhitespaceOnCurrentLine
				&& readCharacter != ' '
				&& readCharacter != '\t'
				&& readCharacter != '\n') {
			hasOnlyReadWhitespaceOnCurrentLine = false;
		}
	} while (readCharacter != EOF);
	fclose(file);
	if (errorOccured) {
		fprintf(stderr, "malformed %s\n", fileName);
		destroyConfig(config);
		exit(EXIT_FAILURE);
	}
	if (config->javaProperties->runContext->srcFolder == NULL) {
		copyStringInto(
				DEFAULT_SRC_FOLDER,
				&config->javaProperties->runContext->srcFolder);
	}
	return config;
}

/**
 * Creates a Config from the jam.ini config file of the project.
 * Memory will be allocated for this, so it has to be freed manually.
 */
Config *getConfigOfProject() {
	return getConfigOfFile(CONFIG_FILE);
}

/**
 * Prepends each relative entry in the given classpath with the given prefix.
 */
void prependRelativeClasspathEntries(char *classpath, const char *prefix) {
	char *newClasspath = NULL;
	int prefixSize = strlen(prefix) + strlen(PATH_SEPARATOR);
	int classpathSize = strlen(classpath);
	char *lastFound = NULL;
	char *found = classpath;
	do {
		if (found[0] == '/') {
			continue;
		}
		classpathSize += prefixSize;
		if (newClasspath == NULL) {
			newClasspath = malloc(classpathSize + 1);
			strncpy(newClasspath, classpath, found - classpath);
		} else {
			newClasspath = realloc(newClasspath, classpathSize + 1);
			strncat(newClasspath, lastFound, found - lastFound);
		}
		strcat(newClasspath, prefix);
		strcat(newClasspath, PATH_SEPARATOR);
		lastFound = found;
	} while ((found = strchr(found, ':')) == NULL ? false : found++);
	if (lastFound != NULL) {
		strcat(newClasspath, lastFound);
	}
	free(classpath);
	classpath = newClasspath;
}

/**
 * Creates a Config from the jam.ini config file,
 * located in the download directory of the given Dependency.
 * This can only work, if the Dependency has been fetched beforehand,
 * otherwise the getConfigOfFile function will abort.
 * Memory will be allocated for this, so it has to be freed manually.
 */
Config *getConfigOfDependency(struct Dependency *dependency) {
	char *pathToDependency = getPathToDependency(dependency);
	char *configPath = joinStrings(
			PATH_SEPARATOR,
			2,
			pathToDependency,
			CONFIG_FILE);
	Config *config = getConfigOfFile(configPath);
	free(configPath);
	if (config->javaProperties->runContext->classpath != NULL) {
		prependRelativeClasspathEntries(
				config->javaProperties->runContext->classpath,
				pathToDependency);
	}
	config->javaProperties->runContext->srcFolder = joinStrings(
			PATH_SEPARATOR,
			2,
			pathToDependency,
			config->javaProperties->runContext->srcFolder);
	free(pathToDependency);
	copyStringInto(
			config->javaProperties->runContext->srcFolder,
			&config->javaProperties->srcFolders);
	return config;
}

/**
 * Creates JavaProperties with the recursively merged values
 * of the given Config and the Configs of all Dependencies.
 */
JavaProperties *buildMergedJavaProperties(Config *config) {
	JavaProperties *mergedProperties = createJavaProperties();
	struct Dependency *dependency = config->dependency;
	mergeJavaPropertiesInto(config->javaProperties, mergedProperties);
	if (config->javaProperties->runContext->srcFolder != NULL) {
		copyStringInto(
				config->javaProperties->runContext->srcFolder,
				&mergedProperties->runContext->srcFolder);
	}
	if (config->javaProperties->testContext->srcFolder != NULL) {
		copyStringInto(
				config->javaProperties->testContext->srcFolder,
				&mergedProperties->testContext->srcFolder);
	}
	if (config->javaProperties->binFolder != NULL) {
		copyStringInto(
				config->javaProperties->binFolder,
				&mergedProperties->binFolder);
	}
	if (config->javaProperties->runContext->mainClass != NULL) {
		copyStringInto(
				config->javaProperties->runContext->mainClass,
				&mergedProperties->runContext->mainClass);
	}
	if (config->javaProperties->testContext->mainClass != NULL) {
		copyStringInto(
				config->javaProperties->testContext->mainClass,
				&mergedProperties->testContext->mainClass);
	}
	while (dependency != NULL) {
		Config *dependencyConfig = getConfigOfDependency(dependency);
		JavaProperties *mergedDependencyProperties = buildMergedJavaProperties(
				dependencyConfig);
		mergeJavaPropertiesInto(mergedDependencyProperties, mergedProperties);
		destroyJavaProperties(mergedDependencyProperties);
		destroyConfig(dependencyConfig);
		dependency = dependency->nextDependency;
	}
	return mergedProperties;
}

