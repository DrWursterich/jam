
#ifndef JAM_H
#define JAM_H

#include <argp.h>

#include "config.h"

#define COMMAND_COMPILER "javac"
#define COMMAND_SRC_FOLDER_OPEN " $(find \""
#define COMMAND_SRC_FOLDER_CLOSE "\" -name \"*.java\")"
#define COMMAND_BIN_FOLDER_OPEN " -d \""
#define COMMAND_BIN_FOLDER_CLOSE "\""
#define COMMAND_MODULE_PATH_OPEN " --module-path \""
#define COMMAND_MODULE_PATH_CLOSE "\""
#define COMMAND_ADD_MODULES_OPEN " --add-modules \""
#define COMMAND_ADD_MODULES_CLOSE "\""
#define COMMAND_CLASSPATH_OPEN " -cp \""
#define COMMAND_CLASSPATH_CLOSE "\""

#define COMMAND_RUN "java"
#define COMMAND_MAIN_CLASS_OPEN " \""
#define COMMAND_MAIN_CLASS_CLOSE "\""

/**
 * All accepted commands
 */
enum Command {
	INIT,
	BUILD,
	TEST,
	RUN
};

/**
 * All parameters of a command from the invocation
 */
struct CommandArguments {
	enum Command command;
	char *args;
};

/**
 * The parsed data from the command-line invocation.
 */
struct Arguments {
	unsigned short commandCount;
	struct CommandArguments* *commands;
	bool silent;
	bool verbose;
};

void createIni();

char *buildCompileCommandFromProperties(JavaProperties *properties);
char *buildRunCommandFromProperties(JavaProperties *properties);
char *buildTestCommandFromProperties(JavaProperties *properties);

void init(struct CommandArguments *arguments);
void build(struct CommandArguments *arguments);
void test(struct CommandArguments *arguments);
void run(struct CommandArguments *arguments);

static error_t parseOption(int key, char *arg, struct argp_state *state);

int main(const int argc, char* *argv);

#endif

