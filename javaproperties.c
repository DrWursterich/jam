
#include <stdlib.h>
#include <string.h>

#include "javaproperties.h"
#include "stringutils.h"

/**
 * Allocates memory for a new JavaContext,
 * creates it and returns a pointer to it.
 */
JavaContext *createJavaContext() {
	JavaContext *javaContext = malloc(sizeof(JavaContext));
	javaContext->classpath = NULL;
	javaContext->modulePath = NULL;
	javaContext->addModules = NULL;
	javaContext->srcFolder = NULL;
	javaContext->mainClass = NULL;
	return javaContext;
}

/**
 * Frees the memory of the given JavaContext
 * and all of its members.
 */
void destroyJavaContext(JavaContext *context) {
	if (context->classpath != NULL) {
		free(context->classpath);
	}
	if (context->modulePath != NULL) {
		free(context->modulePath);
	}
	if (context->addModules != NULL) {
		free(context->addModules);
	}
	if (context->srcFolder != NULL) {
		free(context->srcFolder);
	}
	if (context->mainClass != NULL) {
		free(context->mainClass);
	}
	free(context);
}

/**
 * Allocates memory for new JavaProperties,
 * creates it and returns a pointer to it.
 * Also creates a JavaContext for both the run-
 * and the test-Context members.
 */
JavaProperties *createJavaProperties() {
	JavaProperties *javaProperties = malloc(sizeof(JavaProperties));
	javaProperties->runContext = createJavaContext();
	javaProperties->testContext = createJavaContext();
	javaProperties->srcFolders = NULL;
	javaProperties->binFolder = NULL;
	javaProperties->arguments = NULL;
	return javaProperties;
}

/**
 * Frees the memory of the given JavaProperties
 * and all of its members.
 */
void destroyJavaProperties(JavaProperties *properties) {
	if (properties->binFolder != NULL) {
		free(properties->binFolder);
	}
	if (properties->srcFolders != NULL) {
		free(properties->srcFolders);
	}
	if (properties->arguments != NULL) {
		free(properties->arguments);
	}
	destroyJavaContext(properties->runContext);
	destroyJavaContext(properties->testContext);
	free(properties);
}

/**
 * Merges the second JavaContext members into the first ones.
 * The source JavaContext will not be modified.
 */
void mergeJavaContextInto(const JavaContext *source, JavaContext *target) {
	mergeStringInto(
			source->classpath,
			CLASSPATH_SEPARATOR,
			&target->classpath);
	mergeStringInto(
			source->modulePath,
			MODULE_PATH_SEPARATOR,
			&target->modulePath);
	mergeStringInto(
			source->addModules,
			ADD_MODULES_SEPARATOR,
			&target->addModules);
}

/**
 * Merges the second JavaProperties members into the first ones.
 * The source JavaProperties will not be modified.
 */
void mergeJavaPropertiesInto(const JavaProperties *source, JavaProperties *target) {
	mergeStringInto(
			source->srcFolders,
			CLASSPATH_SEPARATOR,
			&target->srcFolders);
	mergeJavaContextInto(source->runContext, target->runContext);
	mergeJavaContextInto(source->testContext, target->testContext);
}

