
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "dependency.h"
#include "config.h"
#include "stringutils.h"

/**
 * Creates FileProperties of a file of the file system.
 * Memory will be allocated for this, so it has to be
 * freed manually.
 */
FileProperties *getFilePropertiesOf(const char *file) {
	FileProperties *properties = malloc(sizeof(FileProperties));
	properties->exists = !access(file, F_OK);
	properties->canRead = !access(file, R_OK);
	properties->canWrite = !access(file, W_OK);
	properties->canExecute = !access(file, X_OK);
	return properties;
}

/**
 * Creates a empty Dependency.
 * Memory will be allocated for this, so it has to be
 * freed manually.
 */
struct Dependency *createDependency() {
	struct Dependency *dependency = malloc(sizeof(struct Dependency));
	dependency->name = NULL;
	dependency->url = NULL;
	dependency->version = NULL;
	dependency->nextDependency = NULL;
	return dependency;
}

/**
 * Frees the allocated memory of a Dependency
 * and all its members including all linked Dependencies.
 */
void destroyDependency(struct Dependency *dependency) {
	if (dependency->name != NULL) {
		free(dependency->name);
	}
	if (dependency->url != NULL) {
		free(dependency->url);
	}
	if (dependency->version != NULL) {
		free(dependency->version);
	}
	if (dependency->nextDependency != NULL) {
		destroyDependency(dependency->nextDependency);
	}
	free(dependency);
}

/**
 * Returns the relative path from the root of the project
 * to the given Dependency.
 * Does not include a trailing path-separator.
 * Memory is allocated for this char*,
 * so it has to be freed manually.
 */
char *getPathToDependency(struct Dependency *dependency) {
	return joinStrings(
			PATH_SEPARATOR,
			2,
			DOWNLOAD_OUTPUT_DIR,
			dependency->name);
}

/**
 * Returns the relative path from the root of the project
 * to the jam.ini config file of the given Dependency.
 * Memory is allocated for this char*,
 * so it has to be freed manually.
 */
char *getConfigPathForDependency(struct Dependency *dependency) {
	char *pathToDependency = getPathToDependency(dependency);
	char *configPath = joinStrings(
			PATH_SEPARATOR,
			2,
			pathToDependency,
			CONFIG_FILE);
	free(pathToDependency);
	return configPath;
}

/**
 * Creates a command for a system call to download the given Dependency.
 * In most cases the result will look similar to this:
 * 'git clone -q -c advice.detachedHead=false --depth 1 \
 *    "git@gitlab.com:DrWursterich/conditionals.git" \
 *    ".jamdependencies/" -b "1.0.1"'
 * Memory will be allocated for the result,
 * so it has to be freed manually.
 */
char *buildDownloadCommandFromDependency(struct Dependency *dependency) {
	return joinStrings(
			NULL, /* separator */
			9, /* argc */
			COMMAND_DOWNLOAD_OPEN,
			dependency->url,
			COMMAND_DOWNLOAD_CLOSE,
			COMMAND_DOWNLOAD_OUTPUT_DIR_OPEN,
			dependency->name,
			COMMAND_DOWNLOAD_OUTPUT_DIR_CLOSE,
			COMMAND_DOWNLOAD_VERSION_OPEN,
			dependency->version,
			COMMAND_DOWNLOAD_VERSION_CLOSE);
}

/**
 * Fetches the given Dependency into the .jamdependencies folder.
 * Does nothing if a folder named after this Dependency already exists.
 */
void downloadDependency(struct Dependency *dependency) {
	char *pathToDependency = getPathToDependency(dependency);
	FileProperties *properties = getFilePropertiesOf(pathToDependency);
	bool dependencyIsDownloaded = properties->exists;
	char *command;
	free(properties);
	free(pathToDependency);
	if (dependencyIsDownloaded) {
		return;
	}
	fprintf(stdout, "downloading %s %s from %s ...\n",
			dependency->name,
			dependency->version,
			dependency->url);
	command = buildDownloadCommandFromDependency(dependency);
	fprintf(stdout, "executing \"%s\"\n", command);
	system(command);
	free(command);
}

/**
 * Recursively fetches not yet present Dependencies
 * of this Dependency and all Dependencies linked to it.
 */
void buildDependency(struct Dependency *dependency) {
	char *configPath = getConfigPathForDependency(dependency);
	Config *config = getConfigOfFile(configPath);
	free(configPath);
	dependency = config->dependency;
	while (dependency != NULL) {
		downloadDependency(dependency);
		buildDependency(dependency);
		dependency = dependency->nextDependency;
	}
	destroyConfig(config);
}

