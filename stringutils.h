
#ifndef STRINGUTILS_H
#define STRINGUTILS_H

#define COMMAND_ARGUMENT_OPEN " \""
#define COMMAND_ARGUMENT_CLOSE "\""

char *joinStrings(const char *separator, const int argc, ...);
char *transformArgumentsString(const char *arguments);
void copyStringInto(const char *source, char **target);
void mergeStringInto(const char *source, const char *separator, char **target);
void prependString(char *string, const char *prefix);

#endif

