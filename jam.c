
#include <stdlib.h>
#include <string.h>

#include "jam.h"
#include "stringutils.h"

/**
 * The char*, that describes the program version.
 * Is printed by `jam --version`.
 */
const char *argp_program_version
		= "jam version: 0.0.1\nbuild date: 25.07.2020\n";

/**
 * The documentation of the program.
 * Is printed by `jam --help` amongst other things.
 */
static char documentation[]
		= "JAM -- The Java Artifact Manager";

/**
 * The possible commands, as used to print the usage
 * by `jam` and `jam --usage`.
 */
static char possibleCommands[]
		= "[<init [INIT-OPTION...] | build [BUILD-OPTION...]"
			" | test [TEST-OPTION...] | run [RUN-OPTION...]>...]";

/**
 * The options for the argumentparser.
 * Every argp_option is a flag.
 * If atleast a long or a short notation is given,
 * the flag is usable.
 * Otherwise only the description is printed by `jam --help`.
 * The array has to be terminated by a empty option.
 */
static struct argp_option options[] = {
	{0, 0, 0, 0, "\033[1mtest\033[0m"},
	{"args", 'a', "ARGS", 0,
		"use the given, colon-separated arguments for the java invocation."
			" Colons are escaped by an additional colon"},
	{0, 0, 0, 0, ""},
	{0, 0, 0, 0, "\033[1mrun\033[0m"},
	{"args", 'a', "ARGS", 0,
		"use the given, colon-separated arguments for the java invocation."
			" Colons are escaped by an additional colon"},
	{0, 0, 0, 0, ""},
	{0, 0, 0, 0, "\033[1mglobal\033[0m"},
	/*{"verbose", 'v', 0, 0, "Produce verbose output"},
	{"quiet", 'q', 0, 0, "Don't produce any output"},
	{"silent", 's', 0, OPTION_ALIAS},*/
	{0}
};

/**
 * The argumentparser to parse the arguments
 * this progam is invoked with.
 */
static struct argp argumentParser = {
	options,
	parseOption,
	possibleCommands,
	documentation};

/**
 * Creates a jam.ini config file.
 * The default behavior is to add the following content:
 * package=
 * project=
 * version=1.0
 * author=
 */
void createIni() {
	FILE *ini = fopen(CONFIG_FILE, "w");
	if (ini == NULL) {
		fprintf(stderr, "failed to create %s\n", CONFIG_FILE);
		exit(EXIT_FAILURE);
	}
	fputs("package=\n", ini);
	fputs("project=\n", ini);
	fputs("version=1.0\n", ini);
	fputs("author=\n", ini);
	fclose(ini);
	fprintf(stdout, "%s created\n", CONFIG_FILE);
}

/**
 * Creates a command for a system call to compile the project.
 * In most cases the result will look similar to this:
 * 'javac "$(find "src" -name *.java)" -d "bin" -cp "src:lib/jdbc.jar"'
 * Memory will be allocated for the result,
 * so it has to be freed manually.
 */
char *buildCompileCommandFromProperties(JavaProperties *properties) {
	char *classpath = joinStrings(
			CLASSPATH_SEPARATOR,
			4,
			properties->srcFolders,
			properties->testContext->srcFolder,
			properties->runContext->classpath,
			properties->testContext->classpath);
	char *modulePath = joinStrings(
			MODULE_PATH_SEPARATOR,
			2,
			properties->runContext->modulePath,
			properties->testContext->modulePath);
	char *addModules = joinStrings(
			ADD_MODULES_SEPARATOR,
			2,
			properties->runContext->addModules,
			properties->testContext->addModules);
	bool hasTestSrcFolder = properties->testContext->srcFolder != NULL;
	bool hasBinFolder = properties->binFolder != NULL;
	bool hasModulePath = modulePath != NULL;
	bool hasAddModules = addModules != NULL;
	char *command = joinStrings(
			NULL, /* separator */
			19, /* argc */
			COMMAND_COMPILER,
			COMMAND_SRC_FOLDER_OPEN,
			properties->runContext->srcFolder,
			COMMAND_SRC_FOLDER_CLOSE,
			hasTestSrcFolder ? COMMAND_SRC_FOLDER_OPEN : NULL,
			hasTestSrcFolder ? properties->testContext->srcFolder : NULL,
			hasTestSrcFolder ? COMMAND_SRC_FOLDER_CLOSE : NULL,
			hasBinFolder ? COMMAND_BIN_FOLDER_OPEN : NULL,
			hasBinFolder ? properties->binFolder : NULL,
			hasBinFolder ? COMMAND_BIN_FOLDER_CLOSE : NULL,
			COMMAND_CLASSPATH_OPEN,
			classpath,
			COMMAND_CLASSPATH_CLOSE,
			hasModulePath ? COMMAND_MODULE_PATH_OPEN : NULL,
			hasModulePath ? modulePath : NULL,
			hasModulePath ? COMMAND_MODULE_PATH_CLOSE : NULL,
			hasAddModules ? COMMAND_ADD_MODULES_OPEN : NULL,
			hasAddModules ? addModules : NULL,
			hasAddModules ? COMMAND_ADD_MODULES_CLOSE : NULL);
	free(classpath);
	if (hasModulePath) {
		free(modulePath);
	}
	if (hasAddModules) {
		free(addModules);
	}
	return command;
}

/**
 * Creates a command for a system call to execute the project.
 * In most cases the result will look similar to this:
 * 'java -cp "bin:lib/jdbc.jar" "com.company.project.Application"'
 * Memory will be allocated for the result,
 * so it has to be freed manually.
 */
char *buildRunCommandFromProperties(JavaProperties *properties) {
	char *classpath = joinStrings(
			CLASSPATH_SEPARATOR,
			2,
			properties->binFolder,
			properties->runContext->classpath);
	char *arguments = properties->arguments != NULL
			? transformArgumentsString(properties->arguments)
			: NULL;
	bool hasModulePath = properties->runContext->modulePath != NULL;
	bool hasAddModules = properties->runContext->addModules != NULL;
	bool hasArguments = arguments != NULL;
	char *command = joinStrings(
			NULL, /* no separator */
			14, /* argc */
			COMMAND_RUN,
			COMMAND_CLASSPATH_OPEN,
			classpath,
			COMMAND_CLASSPATH_CLOSE,
			hasModulePath ? COMMAND_MODULE_PATH_OPEN : NULL,
			hasModulePath ? properties->runContext->modulePath : NULL,
			hasModulePath ? COMMAND_MODULE_PATH_CLOSE : NULL,
			hasAddModules ? COMMAND_ADD_MODULES_OPEN : NULL,
			hasAddModules ? properties->runContext->addModules : NULL,
			hasAddModules ? COMMAND_ADD_MODULES_OPEN : NULL,
			COMMAND_MAIN_CLASS_OPEN,
			properties->runContext->mainClass,
			COMMAND_MAIN_CLASS_CLOSE,
			hasArguments ? arguments : NULL);
	free(classpath);
	if (hasArguments) {
		free(arguments);
	}
	return command;
}

/**
 * Creates a command for a system call to execute the tests of the project.
 * In most cases the result will look similar to this:
 * 'java -cp "bin:lib/jdbc.jar:lib/junit-4.jar" \
 *     "org.junit.runner.JUnitCore" "test.AllTests"'
 * Memory will be allocated for the result,
 * so it has to be freed manually.
 */
char *buildTestCommandFromProperties(JavaProperties *properties) {
	char *classpath = joinStrings(
			CLASSPATH_SEPARATOR,
			3,
			properties->binFolder,
			properties->runContext->classpath,
			properties->testContext->classpath);
	char *modulePath = joinStrings(
			MODULE_PATH_SEPARATOR,
			2,
			properties->runContext->modulePath,
			properties->testContext->modulePath);
	char *addModules = joinStrings(
			ADD_MODULES_SEPARATOR,
			2,
			properties->runContext->addModules,
			properties->testContext->addModules);
	char *arguments = properties->arguments != NULL
			? transformArgumentsString(properties->arguments)
			: NULL;
	bool hasModulePath = modulePath != NULL;
	bool hasAddModules = addModules != NULL;
	bool hasArguments = arguments != NULL;
	char *command = joinStrings(
			NULL, /* no separator */
			14, /* argc */
			COMMAND_RUN,
			COMMAND_CLASSPATH_OPEN,
			classpath,
			COMMAND_CLASSPATH_CLOSE,
			hasModulePath ? COMMAND_MODULE_PATH_OPEN : NULL,
			hasModulePath ? modulePath : NULL,
			hasModulePath ? COMMAND_MODULE_PATH_CLOSE : NULL,
			hasAddModules ? COMMAND_ADD_MODULES_OPEN : NULL,
			hasAddModules ? addModules : NULL,
			hasAddModules ? COMMAND_ADD_MODULES_OPEN : NULL,
			COMMAND_MAIN_CLASS_OPEN,
			properties->testContext->mainClass,
			COMMAND_MAIN_CLASS_CLOSE,
			hasArguments ? arguments : NULL);
	free(classpath);
	if (hasModulePath) {
		free(modulePath);
	}
	if (hasAddModules) {
		free(addModules);
	}
	if (hasArguments) {
		free(arguments);
	}
	return command;
}

/**
 * Executes the `init` command:
 * - Creates the jam.ini config file
 * Aborts, if the file already exists.
 */
void init(struct CommandArguments *arguments) {
	FileProperties *configFile = getFilePropertiesOf(CONFIG_FILE);
	bool configFileExists = configFile->exists;
	free(configFile);
	if (configFileExists) {
		fprintf(stderr, "%s already exists\n", CONFIG_FILE);
		exit(EXIT_FAILURE);
	}
	createIni();
}

/**
 * Executes the `build` command:
 * - Recursivly fetches the dependencies, that are not present yet
 * - Merges the JavaProperties of all dependencies
 *     into the JavaProperties of the project
 * - Uses the JavaProperties to create and execute the system call
 *     to compile the project and its dependencies
 */
void build(struct CommandArguments *arguments) {
	Config *config = getConfigOfProject();
	struct Dependency *dependency = config->dependency;
	JavaProperties *mergedProperties;
	char *command;
	while (dependency != NULL) {
		downloadDependency(dependency);
		buildDependency(dependency);
		dependency = dependency->nextDependency;
	}
	mergedProperties = buildMergedJavaProperties(config);
	command = buildCompileCommandFromProperties(mergedProperties);
	destroyJavaProperties(mergedProperties);
	fprintf(stdout, "executing \"%s\"\n", command);
	system(command);
	free(command);
	destroyConfig(config);
}

/**
 * Executes the `test` command:
 * - Merges the JavaProperties of all dependencies
 *     into the JavaProperties of the project
 * - Uses the JavaProperties to create and execute the system call
 *     to run the tests of this project
 * Aborts, if no testClass value is defined in the jam.ini config file
 */
void test(struct CommandArguments *arguments) {
	Config *config = getConfigOfProject();
	JavaProperties *mergedProperties;
	char *command;
	if (config->javaProperties->testContext->mainClass == NULL) {
		fprintf(stderr, "no test class configurated in %s\n", CONFIG_FILE);
		exit(EXIT_FAILURE);
	}
	mergedProperties = buildMergedJavaProperties(config);
	if (arguments->args != NULL) {
		copyStringInto(arguments->args, &mergedProperties->arguments);
	}
	command = buildTestCommandFromProperties(mergedProperties);
	destroyJavaProperties(mergedProperties);
	fprintf(stdout, "executing \"%s\"\n", command);
	system(command);
	free(command);
	destroyConfig(config);
}

/**
 * Executes the `run` command:
 * - Merges the JavaProperties of all dependencies
 *     into the JavaProperties of the project
 * - Uses the JavaProperties to create and execute the system call
 *     to run this project
 * Aborts, if no mainClass value is defined in the jam.ini config file
 */
void run(struct CommandArguments *arguments) {
	Config *config = getConfigOfProject();
	JavaProperties *mergedProperties;
	char *command;
	if (config->javaProperties->runContext->mainClass == NULL) {
		fprintf(stderr, "no main class configurated in %s\n", CONFIG_FILE);
		exit(EXIT_FAILURE);
	}
	mergedProperties = buildMergedJavaProperties(config);
	if (arguments->args != NULL) {
		copyStringInto(arguments->args, &mergedProperties->arguments);
	}
	command = buildRunCommandFromProperties(mergedProperties);
	destroyJavaProperties(mergedProperties);
	fprintf(stdout, "executing \"%s\"\n", command);
	system(command);
	free(command);
	destroyConfig(config);
}

/**
 * Adds new CommandArguments to the end of the commands of Arguments.
 * The CommandArguments to be appended are created from the given Command.
 * Since memory is allocated for the new CommandArguments,
 * they have to be freed manually.
 */
void appendNewCommandArgument(
		struct Arguments *arguments,
		enum Command command) {
	struct CommandArguments *newCommands = malloc(
			sizeof(struct CommandArguments*));
	arguments->commands = realloc(
			arguments->commands,
			sizeof(struct CommandArguments*) * (arguments->commandCount + 1));
	newCommands->args = NULL;
	newCommands->command = command;
	arguments->commands[arguments->commandCount] = newCommands;
	arguments->commandCount += 1;
}

/**
 * Executes the given CommandArguments.
 * Aborts, if the Command is unknown.
 */
void executeCommand(struct CommandArguments *arguments) {
	switch (arguments->command) {
		case INIT:
			init(arguments);
			break;
		case BUILD:
			build(arguments);
			break;
		case TEST:
			test(arguments);
			break;
		case RUN:
			run(arguments);
			break;
		default:
			fprintf(
					stderr,
					"the command 0x%0*x is not valid and cannot be executed\n",
					(int)sizeof(enum Command),
					arguments->command);
			exit(EXIT_FAILURE);
	}
}

/**
 * Function for the argumentparser, that parses a single
 * flag/command/argument from the command-line invocation.
 * Resulting data is stored in the input of the given argp_state.
 */
static error_t parseOption(int key, char *arg, struct argp_state *state) {
	struct Arguments *arguments = state->input;
	struct CommandArguments *currentCommandArgs = arguments->commandCount > 0
			? arguments->commands[arguments->commandCount - 1]
			: NULL;
	if (key == ARGP_KEY_ARG) {
		if (strcmp(arg, "init") == 0) {
			appendNewCommandArgument(arguments, INIT);
		} else if (strcmp(arg, "build") == 0) {
			appendNewCommandArgument(arguments, BUILD);
		} else if (strcmp(arg, "test") == 0) {
			appendNewCommandArgument(arguments, TEST);
		} else if (strcmp(arg, "run") == 0) {
			appendNewCommandArgument(arguments, RUN);
		} else {
			return ARGP_ERR_UNKNOWN;
		}
	} else if (key == ARGP_KEY_END) {
		if (state->arg_num == 0) {
			argp_usage(state);
		}
	} else if (currentCommandArgs == NULL) {
		switch (key) {
			case 'q':
				arguments->silent = true;
				break;
			case 'v':
				arguments->verbose = true;
				break;
			default:
				return ARGP_ERR_UNKNOWN;
		}
	} else switch (currentCommandArgs->command) {
		case INIT:
			switch(key) {
				default:
					return ARGP_ERR_UNKNOWN;
			}
			break;
		case BUILD:
			switch(key) {
				default:
					return ARGP_ERR_UNKNOWN;
			}
			break;
		case TEST:
			switch(key) {
				case 'a':
					currentCommandArgs->args = arg;
					break;
				default:
					return ARGP_ERR_UNKNOWN;
			}
		case RUN:
			switch(key) {
				case 'a':
					currentCommandArgs->args = arg;
					break;
				default:
					return ARGP_ERR_UNKNOWN;
			}
			break;
	}
	return 0;
}

/**
 * Parses all command-line parameters invoked with
 * and executes each resulting Command in order.
 */
int main(const int argc, char* *argv) {
	struct Arguments arguments;
	int i = 0;

	arguments.silent = false;
	arguments.verbose = false;
	arguments.commandCount = 0;
	arguments.commands = NULL;

	argp_parse(&argumentParser, argc, argv, ARGP_IN_ORDER, 0, &arguments);

	while (i < arguments.commandCount) {
		executeCommand(arguments.commands[i++]);
	}

	exit(EXIT_SUCCESS);
}

